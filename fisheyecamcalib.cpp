#include "FishEyeCamCalib.h"


#ifndef _CRT_SECURE_NO_WARNINGS
# define _CRT_SECURE_NO_WARNINGS
#endif

static void help()
{
    cout <<  "This is a camera calibration sample." << endl
         <<  "Usage: calibration configurationFile imagesDirectory"  << endl
         <<  "Near the sample file you'll find the configuration file, which has detailed help of "
             "how to edit it.  It may be any OpenCV supported file format XML/YAML." << endl;
}

static double computeReprojectionErrors( const vector<vector<Point3d> >& objectPoints,
                                         const vector<vector<Point2d> >& imagePoints,
                                         const vector<Vec3d>& rvecs, const vector<Vec3d>& tvecs,
                                         const Mat& cameraMatrix , const Mat& distCoeffs,
                                         vector<float>& perViewErrors)
{
    vector<Point2f> imagePoints2;
    int i, totalPoints = 0;
    double totalErr = 0, err;
    perViewErrors.resize(objectPoints.size());

    for( i = 0; i < (int)objectPoints.size(); ++i )
    {
        projectPoints( Mat(objectPoints[i]), rvecs[i], tvecs[i], cameraMatrix,
                       distCoeffs, imagePoints2);
        err = norm(Mat(imagePoints[i]), Mat(imagePoints2), CV_L2);

        int n = (int)objectPoints[i].size();
        perViewErrors[i] = (float) std::sqrt(err*err/n);
        totalErr        += err*err;
        totalPoints     += n;
    }

    return std::sqrt(totalErr/totalPoints);
}

/*
bool runFishEyeCalibration(int flag, Size imageSize, Mat cameraMatrix, Mat distCoeffs, vector<vector<Point2f> > imagePoints, 
						   vector<Mat> rvecs, vector<Mat> tvecs, vector<float> reprojErrs, double totalAvgErr)
{
	cameraMatrix = Mat::eye(3, 3, CV_64F);

	if( flag & CV_CALIB_FIX_ASPECT_RATIO )
        cameraMatrix.at<double>(0,0) = 1.0;

	distCoeffs = Mat::zeros(8, 1, CV_64F);

    vector<vector<Point3f> > objectPoints(1);
    objectPoints.resize(imagePoints.size(),objectPoints[0]);

	double rms = fisheye::calibrate(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs, fisheye::CALIB_USE_INTRINSIC_GUESS | 
									fisheye::CALIB_RECOMPUTE_EXTRINSIC | fisheye::CALIB_FIX_SKEW | fisheye::CALIB_FIX_K2 |
									fisheye::CALIB_FIX_K3 | fisheye::CALIB_FIX_K4);

	cout << "Re-projection error reported by calibrateCamera: "<< rms << endl;

	bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);


	totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
                                             rvecs, tvecs, cameraMatrix, distCoeffs, reprojErrs);

	return true;

}
*/
/*
vector<Point3f> Create3DChessboardCorners(float w, float h, float squareSize)
{
  
	// This function creates the 3D points of your chessboard in its own coordinate system
	float width = (w-1)*squareSize;
	float height = (h-1)*squareSize;


	vector<Point3f> corners;

	for( int i = 0; i < h; i++ )	
		for( int j = 0; j < w; j++ )				
			corners.push_back(Point3f(float(j*squareSize)-width, float(i*squareSize)-height, 0));
		

  return corners;
}
*/

int main(int argc, char* argv[])
{
	if(argc < 2)
	{
		help();
		return -1;
	}
	
	else
	{						
		const char* inputImageDir = argv[1]; //"C:\\Users\\abgg\\Downloads\\od\\Jakub\\CalibrationImages2"; //

		string xmlFileName = "";

		if(argc > 2)
			xmlFileName = argv[2];					

		bool draw = false;

		if(argc > 3)
			if(atoi(argv[3]) > 0)
				draw = true;

		cout << inputImageDir << endl;

		cout << "input xml file: " << xmlFileName << endl;

		DIR *dir;
		struct dirent *ent;
		
		vector<string> img_fnames;
		vector<string> img_names;
		vector<Mat> img_vec;
		
		vector<vector<Point2f> > imagePoints;		 				

		Mat tmp_img;

		if ((dir = opendir (inputImageDir)) != NULL) 
		{			
			int loop_count = 0;

			/* print all the files and directories within directory */
			while ((ent = readdir (dir)) != NULL) 
			{
				if(loop_count < 2)
				{
					loop_count++;
					continue;
				}
				
				string imstr = inputImageDir;
				imstr.append("\\");
				imstr.append(ent->d_name);
				img_fnames.push_back(imstr);
				img_names.push_back(ent->d_name);

				cout << imstr << endl;				
			}
								
			closedir (dir);
		}
		else
		{
			cout << "directory not found " << endl;

			return -1;
		}							

		Mat timg = imread(img_fnames[0]);

		Size imageSize = timg.size();

		cout << "image size: " << imageSize.width << ", " << imageSize.height << endl;		

		string found_str = "chessboard found: ";
		string not_found_str = "chessboard not found: ";		
		
		Size boardSize = Size(8, 6);

		for(int i = 0; i < img_fnames.size(); i++)
		{
			cout << "Processing for image: " << img_names[i] << endl;

			Mat img = imread(img_fnames[i]);

			if(img.empty())
			{
				cout << "image data not read for " << img_fnames[i] << endl;
				break;
			}

			Mat rimg;
			resize(img, rimg, Size(0,0), 0.3, 0.3);

			vector<Point2f> pointBuf;
			bool found = findChessboardCorners( rimg, boardSize, pointBuf,
				CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);

			for(int j = 0; j < pointBuf.size(); j++)
			{
				pointBuf[j].x /= (float)0.3;
				pointBuf[j].y /= (float)0.3;
			}

			if(found)
			{
				cout << "chessboard found .. " << endl;

				Mat imgGray;
				cvtColor(img, imgGray, COLOR_BGR2GRAY);
				cornerSubPix( imgGray, pointBuf, Size(11,11),
						Size(-1,-1), TermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));

				imagePoints.push_back(pointBuf);
				
				if(draw)
				{
					Mat tmp_img;
					img.copyTo(tmp_img);
				
					drawChessboardCorners(tmp_img, Size(8, 6), Mat(pointBuf), found);

					Mat img2show;
					resize(tmp_img, img2show, Size(0,0), 0.3, 0.3);	

					string winname = found_str;
					winname.append(img_names[i]);

					imshow(winname, img2show);
					char k = waitKey();

					destroyAllWindows;
				
					if(k == 'q')
					{
						break;
						cout << "Calibration process terminated by user" << endl;
						return -2;
					}
				}
				pointBuf.clear();
			}
		
			else
			{
				cout << "Chessboard not found .. skipping!!" << endl;
				if(draw)
				{
					Mat tmp_img;
					resize(img, tmp_img, Size(0,0), 0.3, 0.3);

					string winname = not_found_str;
					winname.append(img_names[i]);

					imshow(winname, tmp_img);
					char k = waitKey();

					destroyAllWindows;

					if(k == 'q')
					{
						break;
						cout << "Calibration process terminated by user" << endl;
						return -2;
					}
				}
			}
				
		}
		

		if(!imagePoints.empty())
		{
			vector<vector<Point2d> > imgPoints(imagePoints.size());
			vector<vector<Point3d> > objPoints;			

			for(size_t j = 0; j < imagePoints.size(); j++)					
				for(size_t k = 0; k < imagePoints[j].size(); k++)
					imgPoints[j].push_back(Point2d((double)imagePoints[j][k].x, (double)imagePoints[j][k].y));			
				
			int width = boardSize.width;                                                                                  
			int height = boardSize.height;                                                                                 
			int sq_sz = 30;       

			double w = (double)((width-1)*sq_sz);
			double h = (double)((height-1)*sq_sz);

			vector<Point3d> obj_Points;

			for (int i = 0; i < height; i++)                                                               					
				for (int j = 0; j < width; j++)                                                          						
					obj_Points.push_back(Point3d(((double)(j * sq_sz)) - w, ((double)(i * sq_sz))-h, 0));

			cout << "obj_points size: " << obj_Points.size() << endl;

			for(int k = 0; k < imagePoints.size(); k++)
				 objPoints.push_back(obj_Points);
						
			
			Mat K; //camera Matrix
			Mat D; //distortion Matrix
			
			vector<Vec3d> rvec;
			vector<Vec3d> tvec;
			vector<float> reprojErrs;
			
			double totalAvgErr = 0;			

			int flag = 0;
			flag |= fisheye::CALIB_RECOMPUTE_EXTRINSIC;
			flag |= fisheye::CALIB_CHECK_COND;
			flag |= fisheye::CALIB_FIX_SKEW;

			cout << "Calibrating ... " << endl;

			cout << "image size: " << imageSize << endl;

			cout << "image points size: " << imagePoints.size() << endl;

			cout << "obj points size: " << objPoints.size() << endl;

			double rms = fisheye::calibrate(objPoints, imgPoints, imageSize, K, D, rvec, tvec, flag);
			

			cout << "camera calibration: " << K << endl << endl << "distortion coeff: " << D << endl;

			cout << "Re-projection error reported by calibrateCamera: "<< rms << endl;

			if(!checkRange(K))
				cout << "K is not in range" << endl;

			if(checkRange(D))
				cout << "D is not in range" << endl;

			//totalAvgErr = computeReprojectionErrors(objPoints, imgPoints, rvec, tvec, K, D, reprojErrs);

			//cout << "total avg error: " << totalAvgErr << endl;

			string undist = "0";

			cout << "Proceed to undistortion (1/0): " << endl;
			getline(cin, undist);

			if(undist == "1")
			{
				//undistort images
				for(int i = 0; i < img_fnames.size(); i++)
				{
					cout << "undistorting image: " << img_names[i] << endl;

					Mat img = imread(img_fnames[i]);

					Mat undist_img;

					undistort(img, undist_img, K, D);

					string undist_fname;
					undist_fname.append(inputImageDir);
					undist_fname.append("//");
					
					size_t dot = img_names[i].find_first_of(".");

					string img_name = img_names[i].substr(0, dot);
					img_name.append("_undist.jpg");

					undist_fname.append(img_name);

					imwrite(undist_fname, undist_img);
				}
			}


		}

	}
	return 1;
}
